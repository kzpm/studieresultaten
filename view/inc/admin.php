<?php

/*
Admin interface for backend operations
*/
require_once 'view/inc/error_reporting.inc';
require_once 'view/header.inc';


echo '
<article class="container">

Wat wilt u doen?<br>
<form action="#" method="POST" name="group" onSelect="submit();">
Acties voor groepen<br><br>
<select name ="selGroup">
<option value="nothing">Niets</option>
 <option value="addGroup">Groep toevoegen</option>
 <option value="modifyGroup">Groep wijzigen</option>
 <option value="delGroup">Groep verwijderen</option>
 </select>

</form>

<form action="#" method="POST" name="student" onSelect="this.form.submit();">
Acties voor studenten<br><br>
<select name ="selStudent">
<option value="nothing">Niets</option>
 <option value="addStudent">Student toevoegen</option>
 <option value="modifyStudent">Student wijzigen</option>
 <option value="delStudent">Student verwijderen</option>
 </select>

</form>

</article>
';

if ( !isset( $_POST['selGroup'] ) ){

    echo 'Groep geselecteerd';
}

if ( isset( $_POST['selStudent'] ) ){

    echo 'Student geselecteerd';
}


 ?>
