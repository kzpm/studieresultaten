<!DOCTYPE HTML>
<head>
<title>Studieresultaten</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href='https://fonts.googleapis.com/css?family=Arbutus Slab' rel='stylesheet'>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" href="view/css/main.css" />
<script>
	setTimeout(function() {
		 $(".wrong").fadeOut("fast");
	}, 1000);
	setTimeout(function() {
		 $(".correct").fadeOut("fast");
	}, 1000);
</script>

<script>
// When the user clicks on <div>, open the popup
function myFunction() {
  var popup = document.getElementById("myPopup");
  popup.classList.toggle("show");
}
</script>

</head>
<body>
  <nav class="menu">
  <ul>
    <li> Studieresultaten NP Groningen TIC3 TIC4</li>
 <li><a href=".?link=index">Start</div></a></li>
 <li><a href=".?link=admin">Admin</a></li>
 <li><a href=".?link=contact">Contact</a></li>
 <li><a href=".?link=weg">Afmelden</a></li>
</ul>
</nav>
