<?php

/*
Admin interface for backend operations
*/
require_once 'view/inc/error_reporting.inc';
require_once 'view/header.inc';
include 'view/addGroup.php';
include 'view/modifyGroup.php';
include 'view/delGroup.php';
include 'view/addStudent.php';
include 'view/modifyStudent.php';
include 'view/delStudent.php';
include 'view/addVak.php';
include 'view/modifyVak.php';
include 'view/delVak.php';
include 'view/addKD.php';
include 'view/modifyKD.php';
include 'view/delKD.php';
include 'view/addBeoordeling.php';
include 'view/modifyBeoordeling.php';
include 'view/delBeoordeling.php';




echo '
<div class="container">

Wat wilt u doen?<br>
<form action="#" method="POST" name="group" onChange="submit();">

<select name ="selGroup">
<option value="nothing">Acties voor groepen</option>
 <option value="addGroup">Groep toevoegen</option>
 <option value="modifyGroup">Groep wijzigen</option>
 <option value="delGroup">Groep verwijderen</option>
 </select>

</form>

<form action="#" method="POST" name="student" onChange="submit();">
<select name ="selStudent">
<option value="nothing">Acties voor studenten</option>
 <option value="addStudent">Student toevoegen</option>
 <option value="modifyStudent">Student wijzigen</option>
 <option value="delStudent">Student verwijderen</option>
 </select>

</form>

<form action="#" method="POST" name="vak" onChange="submit();">
<select name ="selVak">
<option value="nothing">Acties voor vakken</option>
 <option value="addVak">Vak toevoegen</option>
 <option value="modifyVak">Vak wijzigen</option>
 <option value="delVak">Vak verwijderen</option>
 </select>

</form>

<form action="#" method="POST" name="KD" onChange="submit();">
<select name ="selKD">
<option value="nothing">Acties voor keuzedelen</option>
 <option value="addKD">KD toevoegen</option>
 <option value="modifyKD">KD wijzigen</option>
 <option value="delKD">KD verwijderen</option>
 </select>

 <form action="#" method="POST" name="Beoordeling" onChange="submit();">
 <select name ="selBeoordeling">
 <option value="nothing">Acties voor beoordelingen</option>
  <option value="addBeoordeling">Beoordeling toevoegen</option>
  <option value="modifyBeoordeling">Beoordeling wijzigen</option>
  <option value="delBeoordeling">Beoordeling verwijderen</option>
  </select>

</form>

</div>
';

if ( isset( $_POST['selGroup'] ) ){

    switch ($_POST['selGroup'] ) {

      case 'addGroup': $ag =  new np\addGroup();
      break;
      case 'modifyGroup': $ag =new np\modifyGroup();
      break;
      case 'delGroup': $ag =new np\delGroup();
      break;
    }

    //echo $gc->addGroup( $_POST['selGroup'] );

}//fi

if ( isset( $_POST['selStudent'] ) ){

  switch ($_POST['selStudent'] ) {

    case 'addStudent': $ag =new np\addStudent();
    break;
    case 'modifyStudent': $ag =new np\modifyStudent();
    break;
    case 'delStudent': $ag =new np\delStudent();
    break;
  }

} //fi

if ( isset( $_POST['selVak'] ) ){

  switch ($_POST['selVak'] ) {

    case 'addVak': $ag =new np\addVak();
    break;
    case 'modifyVak': $ag =new np\modifyVak();
    break;
    case 'delVak': $ag =new np\delVak();
    break;
  }

} //fi

if ( isset( $_POST['selKD'] ) ){

  switch ($_POST['selKD'] ) {

    case 'addKD': $ag =new np\addKD();
    break;
    case 'modifyKD': $ag =new np\modifyKD();
    break;
    case 'delKD': $ag =new np\delKD();
    break;
  }

} //fi

if ( isset( $_POST['selBeoordeling'] ) ){

  switch ($_POST['selBeoordeling'] ) {

    case 'addBeoordeling': $ag =new np\addBeoordeling();
    break;
    case 'modifyBeoordeling': $ag =new np\modifyBeoordeling();
    break;
    case 'delBeoordeling': $ag =new np\delBeoordeling();
    break;
  }

} //fi

 ?>
